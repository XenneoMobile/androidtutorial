package com.mindorks.framework.mediaplayperdemo.videoview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.mindorks.framework.mediaplayperdemo.R;

public class VideoViewFragment extends Fragment {

    private VideoViewViewModel videoViewViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.videoViewViewModel = ViewModelProviders.of(this).get(VideoViewViewModel.class);
        View root = inflater.inflate(R.layout.video_view_fragment, container, false);
        final VideoView videoView = root.findViewById(R.id.videoView);
        this.videoViewViewModel.getUrl().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                videoView.setVideoPath(s);
            }
        });
        return root;
    }
}