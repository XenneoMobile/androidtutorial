package com.mindorks.framework.mediaplayperdemo.videoview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class VideoViewViewModel extends ViewModel {

    private MutableLiveData<String> url;


    public VideoViewViewModel(String dd){
        this.url = new MutableLiveData<>("");
    }
    public void setUrl(String url) {
        if(url==null) {
            this.url = new MutableLiveData<>();
        }
        this.url.setValue(url);
    }
    public LiveData<String> getUrl() {
        return url;
    }
}
