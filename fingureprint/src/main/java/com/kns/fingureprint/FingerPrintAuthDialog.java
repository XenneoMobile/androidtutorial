package com.kns.fingureprint;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

public class FingerPrintAuthDialog extends Dialog implements View.OnClickListener, FingerPrintUIHelper.Callback {

    private FingerprintManager.CryptoObject mCryptoObject;
    private FingerPrintUIHelper mFingerprintUiHelper;
    private Context context;

    public Button cancelBtn;

    public FingerPrintAuthDialog(@NonNull Context context, FingerprintManager.CryptoObject cryptoObject) {
        super(context);
        this.context = context;
        this.mCryptoObject = cryptoObject;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finger_print_auth_dialog);
        this.findViewById(R.id.footerDivider).setVisibility(View.GONE);
        loadFooter();
        this.setCanceledOnTouchOutside(false);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mFingerprintUiHelper = new FingerPrintUIHelper(
                context.getSystemService(FingerprintManager.class),
                (ImageView) this.findViewById(R.id.fingerprint_icon),
                (TextView) this.findViewById(R.id.fingerprint_status), this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.cancelButton) {
            dismiss();
            logout();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void logout() {

    }

    protected void loadFooter() {
        findViewById(R.id.btn_ok_layout).setVisibility(View.GONE);
        findViewById(R.id.btn_cancel_layout).setVisibility(View.VISIBLE);
        this.cancelBtn = this.findViewById(R.id.cancelButton);
        this.cancelBtn.setOnClickListener(this);
    }

    @Override
    public void onAuthenticated() {
        dismiss();
    }

    @Override
    public void onError(int errMsgId, CharSequence errString) {
        if (errMsgId == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
            dismiss();
            Toast.makeText(getContext(),"Too many failed attempts, Please login through Email id and Password."
            ,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mFingerprintUiHelper.startListening(mCryptoObject);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFingerprintUiHelper.stopListening();
    }

    public void startAuthListener() {
        if (mFingerprintUiHelper != null) {
            mFingerprintUiHelper.startListening(mCryptoObject);
        }
    }

    public void stopAuthListener() {
        if (mFingerprintUiHelper != null) {
            mFingerprintUiHelper.stopListening();
        }
    }
}
