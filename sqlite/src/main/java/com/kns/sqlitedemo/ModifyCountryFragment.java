package com.kns.sqlitedemo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ModifyCountryFragment extends Fragment implements OnClickListener {

    private EditText titleText;
    private Button updateBtn, deleteBtn;
    private EditText descText;

    private long _id;

    private DBManager dbManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_modify_record,container,false);
        initView(view);
        display();
        return view;
    }
    private void initView(View view){
        this.titleText =  view.findViewById(R.id.subject_edittext);
        this.descText =  view.findViewById(R.id.description_edittext);
        this.updateBtn =  view.findViewById(R.id.btn_update);
        this.deleteBtn =  view.findViewById(R.id.btn_delete);
    }
    private void display(){
        this.dbManager = new DBManager(getActivity());
        this.dbManager.open();
        Bundle intent = getArguments();
        String id = intent.getString("id");
        String name = intent.getString("title");
        String desc = intent.getString("desc");

        this._id = Long.parseLong(id);
        this.titleText.setText(name);
        this.descText.setText(desc);
        this.updateBtn.setOnClickListener(this);
        this.deleteBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btn_update) {
            String title = titleText.getText().toString();
            String desc = descText.getText().toString();
            this.dbManager.update(_id, title, desc);
            this.returnHome();
        }else  if (v.getId()==R.id.btn_delete) {
            this.dbManager.delete(_id);
            this.returnHome();
        }
    }
    private void returnHome() {
        getFragmentManager().popBackStack();
    }
}
