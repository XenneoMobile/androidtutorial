package com.kns.sqlitedemo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class AddCountryFragment extends Fragment implements OnClickListener {

    private Button addTodoBtn;
    private EditText subjectEditText;
    private EditText descEditText;

    private DBManager dbManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_add_record,container,false);
        initView(view);
        addListener();
        return view;
    }
    private void initView(View view){
        this.subjectEditText =  view.findViewById(R.id.subject_edittext);
        this.descEditText =  view.findViewById(R.id.description_edittext);
        this.addTodoBtn =  view.findViewById(R.id.add_record);
        this.dbManager = new DBManager(getActivity());
        this.dbManager.open();
    }
    private void addListener(){
        this.addTodoBtn.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.add_record) {
            final String name = this.subjectEditText.getText().toString();
            final String desc = this.descEditText.getText().toString();
            this.dbManager.insert(name, desc);
            getFragmentManager().popBackStack();
        }
    }
}