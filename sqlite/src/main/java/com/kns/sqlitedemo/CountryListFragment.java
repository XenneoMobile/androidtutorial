package com.kns.sqlitedemo;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class CountryListFragment extends Fragment {

    private DBManager dbManager;
    private ListView listView;
    private SimpleCursorAdapter adapter;
    final String[] from = new String[] { DatabaseHelper._ID, DatabaseHelper.SUBJECT, DatabaseHelper.DESC };
    final int[] to = new int[] { R.id.id, R.id.title, R.id.desc };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_emp_list,container,false);
        initView(view);
        return view;
    }
    private void initView(View view){
        this.listView =  view.findViewById(R.id.list_view);
        this.listView.setEmptyView(view.findViewById(R.id.empty));
        this.dbManager = new DBManager(getActivity());
        this.dbManager.open();
        Cursor cursor = dbManager.fetch();
        this.adapter = new SimpleCursorAdapter(getActivity(), R.layout.frgment_view_record, cursor, from, to, 0);
        this.adapter.notifyDataSetChanged();
        this.listView.setAdapter(adapter);
        // OnCLickListiner For List Items
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long viewId) {
                TextView idTextView =  view.findViewById(R.id.id);
                TextView titleTextView =  view.findViewById(R.id.title);
                TextView descTextView =  view.findViewById(R.id.desc);

                String id = idTextView.getText().toString();
                String title = titleTextView.getText().toString();
                String desc = descTextView.getText().toString();

                Intent modify_intent = new Intent(getActivity(), ModifyCountryFragment.class);
                modify_intent.putExtra("title", title);
                modify_intent.putExtra("desc", desc);
                modify_intent.putExtra("id", id);
                startActivity(modify_intent);
            }
        });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//        if (id == R.id.add_record) {
//
//            Intent add_mem = new Intent(this, AddCountryFragment.class);
//            startActivity(add_mem);
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

}
