package com.kns.thread;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class PickerType {

    @Retention(RetentionPolicy.SOURCE)                    //declare retention policy source i.e complile time
    @IntDef({OPEN_CAMERA, OPEN_GALLERY,OPEN_OPENVIDEOGALARY})                  // @IntDef value allocation
    public @interface ImageSource { }

    // Must be static final int values for IntDef
    public static final int OPEN_CAMERA = 0;
    public static final int OPEN_GALLERY = 1;
    public static final int OPEN_OPENVIDEOGALARY = 2;

    private int source;

    //As setImageSource has argument type of @ImageSource, parameters passed can only be one of both
    // OPEN_CAMERA or OPEN_GALLERY
    public void setImageSource(@ImageSource int source){
        this.source = source;
    }

//    @ImageSource
//    public int getImageSource(){
//        return OPEN_CAMERA || OPEN_GALLERY;   //Here you can't return simple integer value, It will generate compile time error
//    }

    public static void pickPhoto(@ImageSource int source){
        switch (source){
            case OPEN_CAMERA:
                break;
            case OPEN_GALLERY:
                break;
        }
    }
}
