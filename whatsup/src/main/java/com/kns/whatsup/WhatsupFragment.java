package com.kns.whatsup;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.io.File;


public class WhatsupFragment extends Fragment implements View.OnClickListener {

    private final int PICK_IMAGE=101;
    private final int PICK_VIDEO=102;

    private final String TEXT="TEXT";
    private final String IMAGE="IMAGE";
    private final String VIDEO="VIDEO";
    private final String SCREENSHOT="SCREENSHOT";

    private EditText editText;
    private Button sendDataToWhatsUp,sendImageToWhatsUp,sendScreenShoot,sendVideoToWhatsUp;
    private ImageView selectedImage,selectedVideo;
    private Uri bitmap;
    private Uri videoUri;
    private Uri screenShot;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_whatsup, container, false);
        initView(view);
        addListener();
        this.selectedImage.setImageURI(this.bitmap);
        this.selectedVideo.setImageURI(this.videoUri);
        return view;
    }
    private void initView(View view){
        this.editText=view.findViewById(R.id.editTxt);
        this.sendDataToWhatsUp=view.findViewById(R.id.sendDataToWhatsUp);
        this.selectedImage=view.findViewById(R.id.selectedImage);
        this.sendImageToWhatsUp=view.findViewById(R.id.sendImageToWhatsUp);
        this.sendScreenShoot=view.findViewById(R.id.sendScreenShoot);
        this.sendVideoToWhatsUp=view.findViewById(R.id.sendVideoToWhatsUp);
        this.selectedVideo=view.findViewById(R.id.selectedVideo);
    }
    private void addListener(){
        this.sendDataToWhatsUp.setOnClickListener(this);
        this.sendImageToWhatsUp.setOnClickListener(this);
        this.sendScreenShoot.setOnClickListener(this);
        this.sendVideoToWhatsUp.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.sendScreenShoot){
            if(getView()!=null) {
                takeScreenShot(getView());
            }
        }else if(v.getId()==R.id.sendImageToWhatsUp){
            if(bitmap==null){
                openImagePicker();
            }else {
                sendImageFile();
            }
        }else if(v.getId()==R.id.sendVideoToWhatsUp){
            if(videoUri==null){
                openVideoPicker();
            }else{
                sendVideoFile();
            }
        }else {
            sendtextMessage();
        }
    }

    private void sendtextMessage(){
        send(TEXT);
    }
    private void sendImageFile(){
        send(IMAGE);
    }

    public Bitmap createVideoThumbNail(String path){
        return ThumbnailUtils.createVideoThumbnail(path,MediaStore.Video.Thumbnails.MICRO_KIND);
    }
    private void send(String type){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setPackage("com.whatsapp");
        switch (type){
            case IMAGE:
                if(bitmap==null){
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
                    return;
                }
                sendIntent.setType("image/*");
                sendIntent.putExtra(Intent.EXTRA_STREAM, bitmap);
                break;
            case VIDEO:
                sendIntent.setType("video/*");
                sendIntent.putExtra(Intent.EXTRA_STREAM, videoUri);
                break;
            case SCREENSHOT:
                if(screenShot==null){
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
                    return;
                }
                sendIntent.setType("image/*");
                sendIntent.putExtra(Intent.EXTRA_STREAM, screenShot);
                break;
            case TEXT:
                if(this.editText.getText().toString().trim().length()==0){
                    Toast.makeText(getContext(),"Empty Text",Toast.LENGTH_LONG).show();
                    return;
                }
                sendIntent.putExtra(Intent.EXTRA_TEXT, editText.getText().toString());
                sendIntent.setType("text/plain");
                break;
        }
        startActivity(Intent.createChooser(sendIntent, "Share"));
    }
    private void sendVideoFile(){
        send(VIDEO);
    }

    private void takeScreenShot(View view) {
        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
        view.buildDrawingCache();
        if(view.getDrawingCache() == null) {
            return;
        }
        Bitmap snapshot = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        view.destroyDrawingCache();
        String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), snapshot, "Title", null);
        screenShot= Uri.parse(path);
        send(SCREENSHOT);
    }

    private void openImagePicker(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),PICK_IMAGE);
    }
    private void openVideoPicker(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("video/*");
        startActivityForResult(Intent.createChooser(intent, "Select Video"),PICK_VIDEO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == PICK_IMAGE || requestCode==PICK_VIDEO) {
                Uri selectedImageUri = data.getData();
                final String path = getPathFromURI(selectedImageUri);
                System.out.println("path "+path);
                if (path != null) {
                    File f = new File(path);
                    selectedImageUri = Uri.fromFile(f);
                }
                if(requestCode==PICK_IMAGE) {
                    bitmap = selectedImageUri;
                    selectedImage.setImageURI(selectedImageUri);
                }else{
                    videoUri=selectedImageUri;
                    selectedVideo.setImageURI(videoUri);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }
    private String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor!=null && cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        if(cursor!=null) {
            cursor.close();
        }
        return res;
    }
}
