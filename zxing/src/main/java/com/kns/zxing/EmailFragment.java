package com.kns.zxing;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class EmailFragment extends Fragment implements View.OnClickListener {

    private EditText inSubject, inBody;
    private TextView txtEmailAddress;
    private Button btnSendEmail;
    private String emailAddress;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.emailAddress=getArguments().getString("email_address");
        View view=inflater.inflate(R.layout.activity_email,container,false);
        initViews(view);
        return view;
    }


    private void initViews(View view) {
        this.inSubject = view.findViewById(R.id.inSubject);
        this.inBody = view.findViewById(R.id.inBody);
        this.txtEmailAddress = view.findViewById(R.id.txtEmailAddress);
        this.btnSendEmail = view.findViewById(R.id.btnSendEmail);
        if (this.emailAddress != null) {
            this.txtEmailAddress.setText("Recipient : " + emailAddress);
        }
        this.btnSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{txtEmailAddress.getText().toString()});
                intent.putExtra(Intent.EXTRA_SUBJECT, inSubject.getText().toString().trim());
                intent.putExtra(Intent.EXTRA_TEXT, inBody.getText().toString().trim());
                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnTakePicture) {
            startActivity(new Intent(getActivity(), PictureBarcodeFragment.class));
        }else if(v.getId()==R.id.btnScanBarcode){
            startActivity(new Intent(getActivity(), ScannedBarcodeActivity.class));
        }
    }
}
