package com.kns.zxing;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class ScannQRCodeFragment extends Fragment implements View.OnClickListener {
    private Button zxingIntegration,cammeraIntegration;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_scann_qrcode,container,false);
        initView(view);
        addListener();
        return view;
    }
    private void initView(View view){
        this.zxingIntegration=view.findViewById(R.id.zxingIntegration);
        this.cammeraIntegration=view.findViewById(R.id.cammeraIntegration);
    }
    private void addListener(){
        this.zxingIntegration.setOnClickListener(this);
        this.cammeraIntegration.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.zxingIntegration){
            openZxingScanner();
        }else if(v.getId()==R.id.cammeraIntegration){
            openCammeraScanner();
        }
    }
    private void openZxingScanner(){
        System.out.println("AAAAAAAAAAAAAAAAa");
        IntentIntegrator qrScan = new IntentIntegrator(getActivity());
        qrScan.setOrientationLocked(false);
        qrScan.setCameraId(0);
        qrScan.initiateScan();
    }
    private void openCammeraScanner(){

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        System.out.println("AAAAAAAWWWWWWWWWWWAAAAAAAAAaaa");
        if (result != null) {
            if (result.getContents() != null) {
                Toast.makeText(getContext(),result.getContents(),Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
