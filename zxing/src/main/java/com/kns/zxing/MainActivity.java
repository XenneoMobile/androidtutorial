package com.kns.zxing;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class MainActivity extends Fragment implements View.OnClickListener {

    private Button btnTakePicture, btnScanBarcode;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.activity_main, container,false);
        initViews(view);
        return view;
    }
    private void initViews(View view) {
        this.btnTakePicture = view.findViewById(R.id.btnTakePicture);
        this.btnScanBarcode = view.findViewById(R.id.btnScanBarcode);
        this.btnTakePicture.setOnClickListener(this);
        this.btnScanBarcode.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnTakePicture) {
            startActivity(new Intent(getActivity(), PictureBarcodeFragment.class));
        }else if(v.getId()==R.id.btnScanBarcode) {
            startActivity(new Intent(getActivity(), ScannedBarcodeActivity.class));
        }
    }
}
