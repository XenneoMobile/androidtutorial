//package com.xenneo.dagger2.singlton;
//
//import android.content.Context;
//
//import javax.inject.Inject;
//import javax.inject.Singleton;
//
//@Singleton
//public class MySingleton {
//    @Inject
//    Context context;
//    @Inject
//    MyOtherSingleton myOtherSingleton;
//    @Inject
//    public MySingleton() {
//    }
//    @Inject
//    public MySingleton(Context context, MyOtherSingleton myOtherSingleton) {
//        this.context = context;
//        this.myOtherSingleton = myOtherSingleton;
//    }
//}
