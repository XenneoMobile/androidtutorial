package com.xenneo.dagger2.constructorinjection;

public class MainClass {
    public static void main(String[] args){
        CarComponent carComponent=DaggerCarComponent.create();
        Car car=carComponent.getCar();
        car.drive();
    }
}
