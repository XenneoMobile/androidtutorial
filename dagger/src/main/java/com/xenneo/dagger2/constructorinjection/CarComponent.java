package com.xenneo.dagger2.constructorinjection;

import dagger.Component;

@Component
public interface CarComponent {
    Car getCar();
}
