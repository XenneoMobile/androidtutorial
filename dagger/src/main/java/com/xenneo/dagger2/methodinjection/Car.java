package com.xenneo.dagger2.methodinjection;

import javax.inject.Inject;

public class Car {
    private Engine engine;
    private Wheel wheel;

    @Inject
    public Car(Engine engine, Wheel wheel){
        this.engine=engine;
        this.wheel=wheel;
    }
    @Inject// Call just after constructor dont need to call manually
    public void drive(){
        System.out.println("Driving......");
    }
}
