package com.xenneo.dagger2.methodinjection;

import javax.inject.Inject;

public class Engine {
    @Inject
    public Engine(){

    }
    @Inject
    public void display(){
        System.out.println("Engine Display Method called");
    }
}
