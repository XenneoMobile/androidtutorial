package com.xenneo.dagger2.methodinjection;

import dagger.Component;

@Component
public interface CarComponent {
    void inject(MainClass mainClass);
}
