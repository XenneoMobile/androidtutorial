package com.xenneo.dagger2.fieldinjection;


import javax.inject.Inject;

public class MainClass {
    @Inject Car car;
    @Inject Engine engine;
    public static void main(String[] args){
        new MainClass().display();
    }
    public void display(){
        CarComponent carComponent=DaggerCarComponent.create();
        carComponent.inject(MainClass.this);
        car.drive();
        engine.display();
    }
}
