package com.xenneo.dagger2.fieldinjection;

import javax.inject.Inject;

public class Engine {
    @Inject
    public Engine(){

    }
    public void display(){
        System.out.println("Engine Display Method called");
    }
}
