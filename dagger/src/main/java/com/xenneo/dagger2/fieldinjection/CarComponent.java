package com.xenneo.dagger2.fieldinjection;

import dagger.Component;

@Component
public interface CarComponent {
    void inject(MainClass mainClass);
}
