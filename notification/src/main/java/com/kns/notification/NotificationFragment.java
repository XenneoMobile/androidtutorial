package com.kns.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RemoteViews;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import java.net.URL;

public class NotificationFragment extends Fragment implements View.OnClickListener {

    private Button showNotiFication,showOnscreenNotiFication,showClubNotiFication,showCustomNotiFication;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_notification,container,false);
        initView(view);
        addListener();
        return view;
    }
    private void initView(View view){
        this.showNotiFication=view.findViewById(R.id.showNotiFication);
        this.showOnscreenNotiFication=view.findViewById(R.id.customNotification);
        this.showClubNotiFication=view.findViewById(R.id.showImageNotiFication);
        this.showCustomNotiFication=view.findViewById(R.id.showNotifWithReply);
    }
    public void addListener(){
        this.showNotiFication.setOnClickListener(this);
        this.showOnscreenNotiFication.setOnClickListener(this);
        this.showClubNotiFication.setOnClickListener(this);
        this.showCustomNotiFication.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.showNotiFication){
            showNotiFication();
        }else  if(v.getId()==R.id.customNotification){
            customNotification();
        }else  if(v.getId()==R.id.showImageNotiFication){
            showNotificationWithImage();
        }else  if(v.getId()==R.id.showNotifWithReply){
            showCustomNotiFication();
        }
    }
    int idNumber;
    private void showNotiFication(){
        createChanelID();
        NotificationCompat.Builder mBuilder = mBuilder();
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(appName);
        for (int i=1;i<10;++i) {
            inboxStyle.addLine(1+"");
        }
        mBuilder.setStyle(inboxStyle);
        mBuilder.setAutoCancel(true);
        NotificationManagerCompat notificationManagernew = NotificationManagerCompat.from(getActivity());
        notificationManagernew.notify(++idNumber, mBuilder.build());
    }
    private void customNotification(){
        createChanelID();
        final NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(getActivity());
        final String title="Notification !!!";
        final NotificationCompat.Builder mBuilder =mBuilder();
        final RemoteViews contentView = new RemoteViews(getActivity().getPackageName(), R.layout.remote_view);
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(appName);
        for (int i=1;i<10;++i) {
            inboxStyle.addLine(1+"");
        }
        mBuilder.setStyle(inboxStyle);
        new ImageLoader(){
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                System.out.println(bitmap==null);
                contentView.setImageViewBitmap(R.id.imageView, bitmap);
                contentView.setTextViewText(R.id.message, title);
                mBuilder.setContent(contentView);
                mNotificationManager.notify(++idNumber, mBuilder.build());
            }
        }.execute("https://fsb.zobj.net/crop.php?r=F6eiUCebnchjGGQM554oXSZslwZ9OHO6flYADZM1qozsCw7HUlaqWUULyKK6EBH1wDHWf4YuIza3EswEoOws2hXQZAiQ-re8y8p59RHnoFYnJ_uj-Z7iCL5pqzk9FwTZGiFczjRYeaFe4rD18mbWbEcWyyEUjHLwguSx3BvltYyPG5oeN4_lYO1ylDQ");
    }
    String chanelID="ABC";
    private void showNotificationWithImage(){
        createChanelID();
        final NotificationCompat.Builder mBuilder = mBuilder();
        new ImageLoader(){
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                System.out.println(bitmap==null);
                mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigLargeIcon(bitmap).setSummaryText("Message Summey text"));
                mBuilder.setAutoCancel(true);
                NotificationManagerCompat notificationManagernew = NotificationManagerCompat.from(getActivity());
                notificationManagernew.notify(++idNumber, mBuilder.build());
            }
        }.execute("https://fsb.zobj.net/crop.php?r=F6eiUCebnchjGGQM554oXSZslwZ9OHO6flYADZM1qozsCw7HUlaqWUULyKK6EBH1wDHWf4YuIza3EswEoOws2hXQZAiQ-re8y8p59RHnoFYnJ_uj-Z7iCL5pqzk9FwTZGiFczjRYeaFe4rD18mbWbEcWyyEUjHLwguSx3BvltYyPG5oeN4_lYO1ylDQ");
    }
    private void showCustomNotiFication(){

    }
    String appName="Notification Demo";
    private void createChanelID(){
        NotificationManager notificationManager =(NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager==null){
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = notificationManager.getNotificationChannel(chanelID);
            if (mChannel == null) {
                mChannel = new NotificationChannel(chanelID, appName, NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(mChannel);
                mChannel.enableLights(true);
                mChannel.setLightColor(Color.GREEN);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            }
        }
    }
    private NotificationCompat.Builder mBuilder(){
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getActivity(), chanelID);
        mBuilder.setSmallIcon(R.drawable.notification_icon);
        mBuilder.setContentTitle("Image Notification");
        mBuilder.setContentText("Hi, this is image notification!");
        mBuilder.setTicker("Notification Demo image Notification Received." );
        mBuilder.setColor(getResources().getColor(R.color.colorAccent));
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (alarmSound != null) {
            mBuilder.setSound(alarmSound);
        }
        mBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
        mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        return mBuilder;
    }
   private class  ImageLoader extends AsyncTask<String, Void, Bitmap>{
        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                return BitmapFactory.decodeStream(new URL(strings[0]).openConnection().getInputStream());
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Bitmap notification) {
            super.onPostExecute(notification);
        }
    }
}